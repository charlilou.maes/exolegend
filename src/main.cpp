#include "gladiator.h"
Gladiator* gladiator;
void reset();
void setup()
{
    // instanciation de l'objet gladiator
    gladiator = new Gladiator();
    // enregistrement de la fonction de reset qui s'éxecute à chaque fois avant qu'une partie commence
    gladiator->game->onReset(&reset);
    // positionner le robot aux coordonnées de la première base (0.1; 1.6)
    Position initialPosition = {0.1, 1.6, 0};
    gladiator->game->enableFreeMode(RemoteMode::OFF, initialPosition);
    // initialisation d'une arme en mode SERVO (brancher un servomoteur) sur la pin M1
    gladiator->weapon->initWeapon(WeaponPin::M1, WeaponMode::SERVO);
    gladiator->weapon->setTarget(WeaponPin::M1, 90);
    delay(200);
    // lorsque le robot commencera en free mode sa position de départ sera la valeur de initalPosition
    // Seuls les encodeurs seront utilisés pour calculer la position du robot (elle sera moins précise que celle lorsque le robot est connecté à l'arène)
    // IL est toujours possible de connecter l'outil de debugage minotor au robot lorque celui ci est en free mode
    // Le free mode n'est pas utilisable en simulation
}

void reset()
{
    // fonction de reset:
    // initialisation de toutes vos variables avant le début d'un match
}
void avancer(float speed, float duree)
{
    gladiator->log("Appel de la fonction de avancer");
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, speed); // controle de la roue droite
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, speed);  // control de la roue gauche
    delay(duree);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 0); // stop la roue
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 0);  // stop la roue
}
void reculer(float speed, float duree)
{
    gladiator->log("Appel de la fonction de reculer");
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, -speed); // controle de la roue droite
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, -speed);  // control de la roue gauche
    delay(duree);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 0); // stop la roue
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 0);  // stop la roue
}
void deraper_droite()
{
    gladiator->log("Appel de la fonction déraper droite");
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 1); // control de la roue gauche
    delay(350);
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 0);
}
void deraper_gauche()
{
    gladiator->log("Appel de la fonction déraper gauche");
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 1); // control de la roue gauche
    delay(350);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 0);
}
void tourner_droite()
{
    gladiator->log("Appel de la fonction tourner droite");
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, -1); // controle de la roue droite
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 1);   // control de la roue gauche
    delay(190);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 0); // stop la roue
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 0);  // stop la roue
}
void tourner_gauche()
{
    gladiator->log("Appel de la fonction tourner gauche");
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 1); // controle de la roue droite
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, -1); // control de la roue gauche
    delay(190);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, 0); // stop la roue
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, 0);  // stop la roue
}
void angle_arme(int angle)
{
    gladiator->log("Appel de la fonction tourner arme");
    gladiator->weapon->setTarget(WeaponPin::M1, angle);
}
void arme_fou(int duree)
{
    gladiator->log("Appel de la fonction tourner arme");
    gladiator->weapon->setTarget(WeaponPin::M1, 40);
    delay(120);
    gladiator->weapon->setTarget(WeaponPin::M1, 140);
    delay(120);
}


void loop() {
    if(gladiator->game->isStarted()) { //tester si un match à déjà commencer
        //code de votre stratégie
        gladiator->log("Le jeu a commencé");
    }else {
        gladiator->log("Le jeu n'a pas encore commencé");
    }
    delay(300);

    
        // A savoir avant de lire le code ci-sessous
        // - La méthode des graphes est utilisés pour représenter le labyrinthe
        // - Cette représentation est expliquée en détails dans le GFA chapitre 4.1.6
        // - Il faut être à l'aise avec les pointeurs ;)

        //obtenir la case ou se trouve le robot
        MazeSquare nearestSquare = gladiator->maze->getNearestSquare();
        //vérifier s'il a un mur au dessus
        if(nearestSquare.northSquare == NULL ) { 
            //SI le pointeur pointant vers la case nord est nul, cela veut dire qu'il y a un mur qui empêche de passer
            gladiator->log("Il y a un mur au dessus de la case où se trouve le robot");
            tourner_droite();
        }
        else {
            avancer(1,500);
        }


        delay(1500);
    }
